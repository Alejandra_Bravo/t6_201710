package Data_Structures_Test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;


public class ListaEncadenadaTest extends TestCase{

	private ListaEncadenada<String> lista;
	private Iterator<String> iterador;

	public void setupEscenario1(){
		lista = new ListaEncadenada<String>();
	}
	public void setupEscenario2(){
		lista = new ListaEncadenada<String>();
		lista.agregarElementoFinal("elem1");
		lista.agregarElementoFinal("elem2");
		lista.agregarElementoFinal("elem3");
		lista.agregarElementoFinal("elem4");
		lista.agregarElementoFinal("elem5");
	}
	public void testListaEncadenada(){
		setupEscenario1();
		assertNull("El elemento deber�a ser null",lista.darElemento(0));
	}
	public void testDarElemento(){
		//Cuando se inicializa la lista vac�a
		setupEscenario1();
		String rta1 = lista.darElemento(0);
		assertNull("El elemento no deber�a existir", rta1);

		//Cuando se inicializa la lista con objetos
		setupEscenario2();
		assertEquals("Deber�a devolver el primer elemento","elem1",lista.darElemento(0));
	}
	public void testAgregarElementoFinal(){
		setupEscenario1();
		lista.agregarElementoFinal("elem1");
		assertEquals("El n�mero de elementos deber�a ser 1",1,lista.darNumeroElementos());
		setupEscenario2();
		lista.agregarElementoFinal("elem6");
		assertEquals("El n�mero de elementos deber�a ser seis",6,lista.darNumeroElementos());
	}
	public void testAvanzarSiguientePosicion(){
		setupEscenario1();
		assertFalse("No deber�a avanzar a la siguiente posici�n",lista.avanzarSiguientePosicion());
		lista.agregarElementoFinal("elem1");
		assertFalse("No deber�a avanzar a la siguiente posici�n",lista.avanzarSiguientePosicion());
		setupEscenario2();
		lista.darElemento(0);
		assertTrue("Deber�a avanzar a la siguiente posici�n",lista.avanzarSiguientePosicion());
	}
	public void testDarElementoPosicionActual(){
		setupEscenario1();
		assertNull("No deber�a retornar un elemento",lista.darElementoPosicionActual());
		setupEscenario2();
		assertEquals("Deber�a retornar elem5","elem5",lista.darElementoPosicionActual());
	}
	public void testDarNumeroElementos(){
		setupEscenario2();
		assertEquals("Deber�a retornar cinco",5,lista.darNumeroElementos());
	}
	public void testRetrocederPosicionAnterior(){
		setupEscenario1();
		assertFalse("No deber�a retroceder",lista.retrocederPosicionAnterior());
		lista.agregarElementoFinal("elem1");
		assertFalse("No deber�a retroceder",lista.retrocederPosicionAnterior());
		setupEscenario2();
		assertTrue("Deber�a retroceder una posici�n", lista.retrocederPosicionAnterior());
	}
	public void testIterator(){
		setupEscenario1();
		iterador = lista.iterator();
		assertFalse("Deber�a retornar falso",iterador.hasNext());
		assertNull("Deber�a retornar null",iterador.next());
		setupEscenario2();
		iterador = lista.iterator();
		assertTrue("Deber�a retornar verdadero",iterador.hasNext());
		assertEquals("Deber�a retornar elem1","elem1",iterador.next());
	}
}
