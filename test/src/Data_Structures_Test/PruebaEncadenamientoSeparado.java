package Data_Structures_Test;

import junit.framework.TestCase;
import model.data_structures.EncadenamientoSeparadoTH;

public class PruebaEncadenamientoSeparado extends TestCase  {
	

	private EncadenamientoSeparadoTH<String, Integer> th;
	
	public void setupEscenario1()
	{
		th=new EncadenamientoSeparadoTH<String, Integer>(5);
		
		th.insertar("Llave", 6);
		th.insertar("Llave2", 9);
		th.insertar("Llave3", 12);
		th.insertar("Llave4", 15);
	}
	
	public void testDarValor()
	{
		//Prueba 1
		setupEscenario1();
		int j=th.darValor("Llave4");
		assertEquals("No corresponde con el valor esperado",15,j);
	}
	
	public void testInsertar()
	{
		setupEscenario1();
		th.insertar("Llave5", 5);
		int g=th.darValor("Llave5");
		assertEquals("No corresponde con el valor esperado",5,g);
	}
	
}
