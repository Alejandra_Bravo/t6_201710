package Data_Structures_Test;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase {
	
	private Queue<String> cola;
	public void setUpEscenario1()
	{
		cola = new Queue<String>();
	}
	
	public void setUpEscenario2()
	{
		cola = new Queue<String>();
		cola.enqueue("elem1");
		cola.enqueue("elem2");
		cola.enqueue("elem3");
		cola.enqueue("elem4");
		cola.enqueue("elem5");

	}
	
	public void testQueue()
	{
		setUpEscenario1();
		assertEquals("la cola no deberia ser nula",true,cola!= null);
	}
	
	public void testEnqueue()
	{
		setUpEscenario1();
		assertEquals("deberian haber cero elementos",0,cola.size());
		cola.enqueue("elem1");
		assertEquals("se debio haber agregado un elemento",1,cola.size());
		cola.enqueue("elem2");
		assertEquals("deberian haber 2 elementos",2,cola.size());

	}
	
	public void testDequeue()
	{
		//cuando la cola esta vacia
		setUpEscenario1();
		assertEquals("deberia retornar null",null,cola.dequeue());
		
		//cauando la cola tiene elementos
		setUpEscenario2();
		assertEquals("deberia retornar elem1","elem1",cola.dequeue());
		assertEquals("deberia retornar 4",4,cola.size());
		
		assertEquals("deberia retornar elem2","elem2",cola.dequeue());
		assertEquals("deberia retornar 3",3,cola.size());

		
	}
	
	public void testIsEmpty()
	{
		setUpEscenario1();
		//cuando no hay objetos en la pila
		assertEquals("el metodo deberia retornar true", true ,cola.isEmpty());

		setUpEscenario2();
		//cuando hay elementos en la pila
		assertEquals("el metodo deberia retornar false ya que hay elementos en la pila", false ,cola.isEmpty());
	}
	
	public void testSize()
	{
		setUpEscenario1();
		//cuando hay cero elementos en la pila
		assertEquals("el metodo deberia retornar 0", 0 ,cola.size());

		//cuando hay elementos en la lista
		setUpEscenario2();
		assertEquals("el metodo deberia retornar 5", 5 ,cola.size());
		cola.dequeue();
		assertEquals("el metodo deberia retornar 4", 4 ,cola.size());
	}
	

}
