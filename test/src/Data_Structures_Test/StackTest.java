package Data_Structures_Test;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase {

	private Stack<String> pila;

	public void setUpEscenario1()
	{
		pila = new Stack<String>();
	}

	public void setUpEscenario2()
	{
		pila = new Stack<String>();
		pila.push("elem1");
		pila.push("elem2");
		pila.push("elem3");
		pila.push("elem4");
		pila.push("elem5");
	}

	public void testStack()
	{
		setUpEscenario1();
		assertTrue("la pila deberia existir", pila != null);
	}

	public void testIsEmpty()
	{
		setUpEscenario1();
		//cuando no hay objetos en la pila
		assertEquals("el metodo deberia retornar true", true ,pila.isEmpty());

		setUpEscenario2();
		//cuando hay elementos en la pila
		assertEquals("el metodo deberia retornar false ya que hay elementos en la pila", false ,pila.isEmpty());
	}

	public void testSize()
	{
		setUpEscenario1();
		//cuando hay cero elementos en la pila
		assertEquals("el metodo deberia retornar 0", 0 ,pila.size());

		//cuando hay elementos en la lista
		setUpEscenario2();
		assertEquals("el metodo deberia retornar 5", 5 ,pila.size());
		pila.push("elem6");
		assertEquals("el metodo deberia retornar 6", 6 ,pila.size());

	}

	public void testPush()
	{
		setUpEscenario1();
		assertEquals("el metodo deberia retornar 0", 0 ,pila.size());
		pila.push("elem1");
		assertEquals("el metodo deberia retornar 1", 1 ,pila.size());
		pila.push("elem2");
		assertEquals("el metodo deberia retornar 2", 2 ,pila.size());
		pila.push("elem3");
		assertEquals("el metodo deberia retornar 2", 3 ,pila.size());

	}

	public void testPop()
	{
		setUpEscenario1();
		//cuando no hay nada en la pila
		assertEquals("el metodo deberia retornar null", null ,pila.pop());
		pila.push("elem1");
		assertEquals("el metodo deberia retornar elem1", "elem1" ,pila.pop());


		//cuando hay elementos en la pila
		setUpEscenario2();
		assertEquals("el metodo deberia retornar elem5", "elem5" ,pila.pop());
		assertEquals("el metodo deberia retornar elem4", "elem4" ,pila.pop());

	}




}
