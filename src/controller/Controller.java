package controller;

import model.logic.*;

public class Controller {
	//atributos
	private ClasePruebas a;

	private ManejadorPeliculas b;

	public Controller()
	{

	}

	public void crearManejadorPelis(int i)
	{
		b = new ManejadorPeliculas(i);
	}

	public void buscarPelicula(long id)
	{
		if (b!= null)
		{
			b.buscarPelicula(id);
		}else
		{
			System.out.println("primero debe crear el manejador de peliculas");
		}
	}
	
	public double darPromedio(){
		
		return b.darPromedio();
	}

}
