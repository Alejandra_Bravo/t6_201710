package model.logic;

import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaLlaveValorSecuencial;
 public class ClasePruebas {

	//atributos
	private GeneradorDatos rand;
	
	EncadenamientoSeparadoTH<String, Integer> tablaPrueba;

	//constructor
	/**
	 * crea la clesde de pruebea e inizializa el generador de datos
	 */
	public ClasePruebas()
	{
		rand = new GeneradorDatos();
	}

	//metodos
	
	/**
	 * crea una tabla de prueba con la cantidad de datos dados por parametro
	 * @param cantidadArreglo cuantos slots hay por tabla
	 * @param cantidadDatos cuantos datos seran agregados a la tabla 
	 * @param cantidadString cantidad de a�s van a haber como llave. 
	 */
	public void crearTablaPrueba(int cantidadArreglo, int cantidadDatos, int cantidadString)
	{
		Integer[] numeros = rand.generarNumeros(cantidadDatos);
		String llaves[] = rand.generarCadenas(cantidadString, cantidadDatos);
		tablaPrueba= new EncadenamientoSeparadoTH<>(cantidadArreglo);
		
		for (int i = 0; i < numeros.length; i++) {
			tablaPrueba.insertar(llaves[i], numeros[i]);
		}
	}
	/**
	 * recorre la tabla imprimiendo cada uno de los datos
	 */
	public void recorrerTablaPrueba()
	{
		if (tablaPrueba!= null)
		{
			int a = tablaPrueba.darTabla().length;
			ListaLlaveValorSecuencial<String, Integer>[] tab = tablaPrueba.darTabla();
			for (int i = 0; i < a; i++) {
			tab[i].darCabezaNodo();
			}
		}
	}
	
	public static void main(String[] args){
		ClasePruebas prueba = new ClasePruebas();
		for(int k = 10; k <= 100; k+=10){
			long inicio, total;
			System.out.println("Tama�o: "+k);
			
			System.out.println("Cantidad Datos: "+1000);
			inicio = System.currentTimeMillis();
			prueba.crearTablaPrueba(k,1000,1000);
			total = System.currentTimeMillis() - inicio;
			System.out.println("Tiempo: "+total);
			
			System.out.println("Cantidad Datos: "+5000);
			inicio = System.currentTimeMillis();
			prueba.crearTablaPrueba(k,5000,1000);
			total = System.currentTimeMillis() - inicio;
			System.out.println("Tiempo: "+total);
			
			System.out.println("Cantidad Datos: "+10000);
			inicio = System.currentTimeMillis();
			prueba.crearTablaPrueba(k,10000,1000);
			total = System.currentTimeMillis() - inicio;
			System.out.println("Tiempo: "+total);
		}

	}

}
