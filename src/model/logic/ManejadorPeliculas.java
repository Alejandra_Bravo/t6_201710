package model.logic;

import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import model.data_structures.EncadenamientoSeparadoTH;
import model.vo.VOInfoPelicula;

public class ManejadorPeliculas {

	//------------------------------------------
	//Atributos
	//------------------------------------------

	EncadenamientoSeparadoTH<Long, VOInfoPelicula> tablaPeliculas;

	//------------------------------------------
	//Constructor
	//------------------------------------------
	/**
	 * crea el manejador de peliculas con un tama�o especifico
	 * @param  pNumArreglo numero del tama�o del arreglo
	 */
	public ManejadorPeliculas(int pNumArreglo)
	{

		tablaPeliculas = new EncadenamientoSeparadoTH<>(pNumArreglo);
		cargarPeliculasSR();
	}

	//------------------------------------------
	//Metodos
	//------------------------------------------

	/**
	 * carga las peliculas del archivo json
	 * @return true si se logro cargar, false de lo contrario.
	 */
	public boolean cargarPeliculasSR() {
		long inicio, fin;
		inicio = System.currentTimeMillis();
		try {
			JSONParser parser  = new JSONParser();
			FileReader reader = new FileReader("data/links_json.json");
			JSONArray jsonArray = (JSONArray) parser.parse(reader);
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject ob = (JSONObject) jsonArray.get(i);
				String ids = (String) ob.get("movieId");
				long id = Integer.parseInt(ids);
				JSONObject ob2 = (JSONObject) ob.get("imdbData");
				String Titulo =(String) ob2.get("Title");
				String Director =(String) ob2.get("Director");
				String actrores =(String) ob2.get("Actors");
				String anios = (String) ob2.get("Year");
				anios = anios.substring(0, 4);
				if(anios.contains("N"))
				{
					anios = "0000";
				}
				int anio =Integer.parseInt(anios);
				String ratings = (String)ob2.get("imdbRating");
				if(ratings.contains("N"))
				{
					ratings = "0";
				}
				double rating =Double.parseDouble(ratings);

				System.out.println(" id: " + id + " titulo: " + Titulo +  " a�o: " + anios + " actores: " + actrores + " Director: " + Director);
				tablaPeliculas.insertar(id, new VOInfoPelicula(Titulo, anio, Director, actrores, rating, id));
			}
			System.out.println("---------- Se han cargado correctamente las peliculas: " +jsonArray.size()+ " Peliculas cargadas correctamente -------------------");
			reader.close();
			fin = System.currentTimeMillis() - inicio;
			System.out.println("Tiempo de carga: "+fin);
			return true;
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}		
	}

	/**
	 * muestra en pantalla el objeto VOinfoPelicula 
	 * @param ID id de la pelicula
	 */
	public void buscarPelicula(Long ID)
	{
		long inicio, fin;
		inicio = System.currentTimeMillis();
		VOInfoPelicula a = tablaPeliculas.darValor(ID);
		if (a != null)
		{
			System.out.println("Titulo: " + a.getTitulo());
			System.out.println("a�io: "+a.getAnio());
			System.out.println("Director: "+a.getDirector());
			System.out.println("Actores: "+a.getActrores());
			System.out.println("ratings: "+a.getRatingIMDB());
			fin = System.currentTimeMillis() - inicio;
			System.out.println("Tiempo de b�squeda: "+fin);
		}
	}
	
	public double darPromedio(){
		double prom = 0;
		int[] tabla = tablaPeliculas.darLongitudListas();
		for (int i = 0; i < tabla.length; i++) {
			prom+=tabla[i];
		}
		prom/=tabla.length;
		
		return prom;
	}
}
