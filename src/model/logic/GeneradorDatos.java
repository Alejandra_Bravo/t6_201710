package model.logic;

import java.security.SecureRandom;

public class GeneradorDatos {
	static SecureRandom rnd = new SecureRandom();
	/**
	 * 
	 * @param k numero limite de caracteres a por muestra
	 * @param N numero de muestras 
	 * @return lista de cadenas de a aleatorias con tama�o n
	 */
	public String[] generarCadenas(int k,int N)
	{
		String a[] = new String[N];
		for (int i = 0; i < a.length; i++) {
			String aIn = "";
			int car = rnd.nextInt(k);
			for (int j = 0; j <car ; j++) {
				aIn +='a';
			}
			a[i] =aIn;
		}
		
		return a;

	}

	public Integer[] generarNumeros(int N)
	{
		Integer a[] = new Integer[N];
		
		for (int i = 0; i < a.length; i++) {
			a[i] = rnd.nextInt(2000);
		}
		
		return a;
		
	}

}
