package model.vo;

public class VOInfoPelicula {

	//
	//Atributos
	//
	/**
	 *  titulo de la pelicula
	 */
	String titulo; 
	
	/**
	 * anio de la pelicula
	 */
	int anio;
	
	/**
	 * director de la pelicula
	 */
	String director;
	
	/**
	 * actores de la pelicula
	 */
	String actrores;
	
	/**
	 * reating de la pelicula
	 */
	double ratingIMDB;
	
	/**
	 * iD de la pelicula
	 */
	long id;
	
	//
	//Constructor
	//
	/**
	 * crea la pelicula con sus atributos
	 * @param pTitulo titulo de la pelicula
	 * @param pAnio a�io de la pelicula
	 * @param pDirector director de la pelicula
	 * @param pActores actores de la pelicula
	 * @param pReating reatin de la pelicula segun IMDB
	 */
	public VOInfoPelicula(String pTitulo, int pAnio, String pDirector, String pActores, double pReating, long pId)
	{
		titulo = pTitulo;
		anio = pAnio;
		director = pDirector;
		actrores = pActores;
		ratingIMDB = pReating;
		id = pId;
	}
	
	//------------------------------------------
	//Metodos
	//------------------------------------------
	
	/**
	 * retorna el id de la pelicula
	 * @return id
	 */
	public long getId() {
		return id;
	}

	/**
	 * retorna el titulo de la pelicula
	 * @return titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * retorna el a�io de la pelicula
	 * @return anio
	 */
	public int getAnio() {
		return anio;
	}

	/**
	 * retorna el director de la pelicula
	 * @return director
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * retorna los actores de la pelicula
	 * @return actores
	 */
	public String getActrores() {
		return actrores;
	}

	/**
	 * retorna el reating de la pelicula
	 * @return ratingIMDB
	 */
	public double getRatingIMDB() {
		return ratingIMDB;
	}
	
	
}
