package model.data_structures;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class ListaLlaveValorSecuencial<K extends Comparable<K>,V> 
{

	
	private NodoSimple<K,V> cabezaNodo;

	private NodoSimple<K,V> nodoActual;

	public void insertar(K llave, V valor) 
	{	
		boolean borrarNodo=false;
		if(valor==null)
			borrarNodo=true;
		NodoSimple<K,V> nuevo = new NodoSimple<K,V>(llave, valor) ;
		NodoSimple<K,V> actual = cabezaNodo;
		if(cabezaNodo == null)
		{
			cabezaNodo = nuevo;
			nodoActual = cabezaNodo;
		}
		else
		{			
			int contador=0;
			boolean agregado=false;
			while(actual!=null)
			{				
				if(actual.darLlave().equals(llave))
				{
					if(!borrarNodo)
					{
						actual.setValor(valor);
						agregado=true;
					}
					else
					{
						eliminarNodo(contador);
						agregado=true;
					}
					break;    					
				}
				actual = actual.darSiguiente();
				contador++;
			}	
			actual=cabezaNodo;
			while(actual!=null&&actual.darSiguiente()!=null)
			{
				actual=actual.darSiguiente();			
			}

			if(!agregado)
			{
				//System.out.println("No estaba agregado "+llave+", se agreg� el nuevo");
				actual.cambiarSiguiente(nuevo);
			}
		}

	}

	
	public K darLlave(int pos) 
	{
		int contador = 0;
		NodoSimple<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(contador == pos)
			{
				return actual.darLlave();
			}
			contador++;
			actual = actual.darSiguiente();
		}
		return null;
	}

	
	public V darValor(K llave)
	{
		NodoSimple<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(actual.darLlave().equals(llave))
			{
				return actual.darValor();
			}			
			actual = actual.darSiguiente();
		}
		return null;

	}

	
	public boolean estaVacia()
	{
		if(cabezaNodo==null)
			return true;
		else
			return false;
	}

	
	public int darTamanio() 
	{
		int numeroElementos = 0;
		if(cabezaNodo!=null)
		{
			NodoSimple<K,V> actual = cabezaNodo;
			while (actual!=null) 
			{
				numeroElementos++;
				actual = actual.darSiguiente();
			}
		}
		return numeroElementos;
	}

	
	public K darLlavePosicionActual() 
	{
		if(nodoActual!=null)
		{
			return nodoActual.darLlave();
		}
		return null;
	}

	
	public boolean avanzarSiguientePosicion() 
	{
		if(nodoActual!=null && nodoActual.darSiguiente()!=null)
		{
			nodoActual = nodoActual.darSiguiente();
			return true;
		}
		return false;
	}

	
	public void cambiarActualACabeza()
	{
		nodoActual = cabezaNodo;
	}

	
	public ListaLlaveValorSecuencial()
	{
		cabezaNodo=null;
		nodoActual=cabezaNodo;
	}

	
	public boolean tieneSiguiente()
	{
		if(nodoActual.darSiguiente()!=null)
		{
			return true;
		}
		return false;
	}

	public NodoSimple<K,V> darCabezaNodo()
	{
		return cabezaNodo;
	}

	public NodoSimple<K,V> darElementoNodoSimple(int pos) 
	{
		int contador = 0;
		NodoSimple<K,V> actual = cabezaNodo;
		while (actual!=null) 
		{
			if(contador == pos)
			{
				return actual;
			}
			contador++;
			actual = actual.darSiguiente();
		}
		return null;
	}

	
	public int darPosActual()
	{
		if(nodoActual.darLlave().equals(cabezaNodo.darLlave()))
		{
			return 0;
		}
		else
		{
			NodoSimple<K,V> n =cabezaNodo.darSiguiente();
			int pos=1;
			while(n!=null)
			{
				if(n.darLlave().equals(nodoActual.darLlave()))
				{
					return pos;
				}
				pos++;
			}
		}
		return -1;
	}

	
	public K eliminarNodo(int pos)
	{
		NodoSimple<K,V> n = null;
		if(cabezaNodo!=null)
		{
			if(darLlave(pos)!=null)
			{
				if( pos == 0)
				{
					if(nodoActual == cabezaNodo)
					{
						nodoActual = nodoActual.darSiguiente();
					}
					// Es EL primer elemento de la lista
					n=cabezaNodo;
					cabezaNodo = cabezaNodo.darSiguiente( );
					return n.darLlave();
				}
				else
				{
					n = darElementoNodoSimple(pos);
					if(nodoActual == darElementoNodoSimple(pos))
					{
						nodoActual = nodoActual.darSiguiente();
					}
					NodoSimple<K,V> anterior = darElementoNodoSimple(pos-1);
					if(anterior!=null)
					{
						darElementoNodoSimple(pos-1).desconectarSiguiente();
					}
					return n.darLlave();
				}
			}
		}
		return null;
	}
	
	
	public K eliminarNodo(K llave)
	{
		int pos = darPosLlave(llave);
		if(pos<0)
		{
			return null;
		}
		NodoSimple<K,V> n = null;
		if(cabezaNodo!=null)
		{
			if(darLlave(pos)!=null)
			{
				if( pos == 0)
				{
					if(nodoActual == cabezaNodo)
					{
						nodoActual = nodoActual.darSiguiente();
					}
					// Es EL primer elemento de la lista
					n=cabezaNodo;
					cabezaNodo = cabezaNodo.darSiguiente( );
					return n.darLlave();
				}
				else
				{
					n = darElementoNodoSimple(pos);
					if(nodoActual == darElementoNodoSimple(pos))
					{
						nodoActual = nodoActual.darSiguiente();
					}
					NodoSimple<K,V> anterior = darElementoNodoSimple(pos-1);
					if(anterior!=null)
					{
						darElementoNodoSimple(pos-1).desconectarSiguiente();
					}
					return n.darLlave();
				}
			}
		}
		return null;
	}
	
	
	public int darPosLlave(K llave)
	{
		NodoSimple<K, V> nodo = cabezaNodo;
		int pos = 0;
		while(nodo != null)
		{
			if(nodo.darLlave().equals(llave))
			{
				return pos;
			}
			pos++;
			nodo = nodo.darSiguiente();
		}
		return -1;
	}

	public void agregarElementoAlInicio(NodoSimple<K,V> nuevo1)
	{
		NodoSimple<K,V> nuevo = nuevo1;
		NodoSimple<K,V> actual = cabezaNodo;
		if(actual == null)
		{
			cabezaNodo = nuevo;
		}
		else
		{
			nuevo.setNext(cabezaNodo);
			cabezaNodo = nuevo;
		}
	}

	
	public boolean tieneLlave(K llave)
	{
		//nodoActual=cabezaNodo;
		NodoSimple<K,V> actual = cabezaNodo;
		if(cabezaNodo == null)
		{
			return false;
		}
		else if(cabezaNodo.darLlave().equals(llave))
		{
			return true;
		}
		else
		{
			while(actual!=null)
			{
				if(actual.darLlave().equals(llave))
				{
					return true;   					
				}
				actual = actual.darSiguiente();
			}	
			return false;
		}
	}

	
	public Iterable<K> llaves()
	{
		return new Iterable<K>()
		{
			@Override
			public Iterator<K> iterator() 
			{
				return llavesK();
			}
		};		
	}
	
	
	public Iterator<K> llavesK()
	{
	    final ListaLlaveValorSecuencial<K, V> lista = this;
	    return new Iterator<K>() 
	    {
	        final NodoSimple<K,V> primero = lista.cabezaNodo;
	        NodoSimple<K,V> actualNodo = null;
	        @Override
	        public boolean hasNext() 
	        {
	            if (lista.estaVacia())
	            {
	                return false;
	            } 
	            else if (actualNodo == null)
	            {
	                return true;
	            } 
	            else if (actualNodo == lista.darElementoNodoSimple(darTamanio()-1))
	            {
	                return false;
	            }
	            return true;
	        }
	        @Override
	        public K next() 
	        {
	            if (lista.estaVacia())
	            {
	                throw new NoSuchElementException();
	            } 
	            else if (actualNodo == null)
	            {
	                this.actualNodo = primero;
	                return actualNodo.darLlave();
	            } 
	            else if (actualNodo.darSiguiente() == null) 
	            {
	                throw new NoSuchElementException();
	            }
	            this.actualNodo = actualNodo.darSiguiente();
	            return actualNodo.darLlave();
	        }
	    };
	}
}
