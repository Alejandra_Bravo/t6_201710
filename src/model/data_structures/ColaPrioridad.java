package model.data_structures;

public class ColaPrioridad  <T extends Comparable<T>>  {
	// atributos
	
		private int max;
		
		private Queue<T> cola;
		
		public void crearCP(int max)
		{
			cola = new Queue<T>();
			this.max = max;
		}
		
		public int darNumElementos()
		{
			return cola.size();
		}
		
		public void agregar(T elemento) throws Exception
		{
			if(max == darNumElementos())
			{
			throw new Exception("la lista esta llena");
			}
			
			cola.enqueue(elemento);
			for (int i = darNumElementos()-1; i > 0 ; i--) {
				if(elemento.compareTo(cola.darElemento(i-1))<0)
				{
					T irem = cola.darElemento(i-1);
					cola.replaceItem(elemento, i-1);
					cola.replaceItem(irem, i);
				}else{
					break;
				}
			}
			

		}
		
		public  T max() 
		{
			
				T aDevolver = cola.darElemento(0);
				cola.eliminaPrimeraPos();
				return aDevolver;
		}
		
		public boolean esVacia()
		{
			return (darNumElementos()==0)? true : false;
		}
		
		public int tamanoMax()
		{
			return this.max;
		}
}
