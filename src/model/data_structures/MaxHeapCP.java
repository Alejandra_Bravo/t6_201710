package model.data_structures;

public class MaxHeapCP <T extends Comparable<T>> {

	// atributos

	private int num;

	private T[] arreglo;

	private int max;

	public void crearCP(int max)
	{
		arreglo = ((T[]) new Comparable[max+1]); ;
		this.max = max; 
	}

	public int darNumElementos()
	{
		return num;
	}

	public void agregar(T elemento)throws Exception
	{

			if(num == max)
			{
				throw new Exception("la lista esta llena");
			}
			arreglo[++num] = elemento;
			swim(num);
	}

	public  T max() 
	{
		if(esVacia())return null;
		T max = arreglo[1];
		exch(1, num--);
		arreglo[num+1] = null;
		sink(1);
		return max;
	}

	public boolean esVacia()
	{
		return num == 0;
	}

	public int tamanoMax()
	{
		return this.max;
	}

	private void swim(int k)
	{
		while (k > 1 && !less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private void sink(int k)
	{
		while (2*k <= num)
		{
			int j = 2*k;
			if (j < num && !less(j, j+1)) j++;
			if (less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private void exch(int k, int j) {
		T temp = arreglo[k];
		arreglo[k]= arreglo[j];
		arreglo[j] = temp;

	}

	private boolean less(int j, int i) {
		return (arreglo[j].compareTo(arreglo[i]))<0;
	}


}
