package model.data_structures;

public class Stack <T> implements Istack<T> {

	ListaEncadenada<T> pila;
	
	public Stack() {	
		pila = new ListaEncadenada<>();
	}

	@Override
	public void push(T item) {
		pila.agregarAlComienzo(item);
	}

	@Override
	public T pop() {
		return pila.quitaAlComienzo();
	}

	@Override
	public boolean isEmpty() {
		return (pila.darNumeroElementos() == 0)? true : false ;
	}

	@Override
	public int size() {
		return pila.darNumeroElementos();
	}

}
