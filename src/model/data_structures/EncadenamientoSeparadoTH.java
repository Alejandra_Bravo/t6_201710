package model.data_structures;
public class EncadenamientoSeparadoTH <K extends Comparable<K>,V> 
{
		
	private static final int CAPACIDAD_INICIAL =10;
	
	private ListaLlaveValorSecuencial<K,V>[] tabla;

	
	private int tamagno;
	
	private int n;

	
	public EncadenamientoSeparadoTH(int m)
	{
		this.tamagno = m;
		tabla = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[m]; 
		for(int i = 0 ; i < m; i++)
		{
			tabla[i] = new ListaLlaveValorSecuencial<K,V>();
		}	
	}

	
	public void ajustarTamagno(int cadenas)
	{
		EncadenamientoSeparadoTH<K, V> temp = new EncadenamientoSeparadoTH<K, V>(cadenas);
		for (int i = 0; i < tamagno; i++) 
		{
			for (K key : tabla[i].llaves()) 
			{
				temp.insertar(key, tabla[i].darValor(key));
			}
		}
		this.tamagno  = temp.tamagno;
		this.n  = temp.n;
		this.tabla = temp.tabla;
	}

	
	public int darTamanio()
	{
		return n;
	}

	
	public boolean estaVacia()
	{
		return darTamanio()  == 0;
	}

	
	public boolean tieneLlave(K llave)
	{
		return darValor(llave)  != null;
	}

	
	public V darValor(K llave)
	{
		if(llave == null)
		{
			return null;
		}
		else
		{
			int i = hash(llave);
			return tabla[i].darValor(llave);
		}
	}

	
	public void insertar(K llave, V valor)
	{
		if (llave == null)
		{
			throw new IllegalArgumentException("Se intento agregar un elemento con llave null");
		}
		if (valor == null) 
		{
			eliminar(llave);
			return;
		}
		// si el tamagno promedio de listas es mayor o igual que diez, se duplica el tamagno de la tabla
		if (n >= 10*tamagno)
		{
			ajustarTamagno(2*tamagno);
		}
		int i = hash(llave);
		if (!tabla[i].tieneLlave(llave)) 
		{
			n++;
		}
		tabla[i].insertar(llave, valor);

	}

	public void eliminar(K llave) 
	{
		if (llave == null)
		{
			return;
		}

		int i = hash(llave);
		if (tabla[i].tieneLlave(llave))
		{
			n--;
		}
		tabla[i].eliminarNodo(llave);

		// disminuir longitud de la tabla si el promedio de listas es <= 2
		if (tamagno > CAPACIDAD_INICIAL && n <= 2*tamagno)
		{
			ajustarTamagno(tamagno/2);
		}
	} 

	
	public int[] darLongitudListas()
	{
		int[] tama=new int[tamagno];

		for(int i=0;i<tabla.length;i++)
		{
			tama[i]=tabla[i].darTamanio();
		}
		return tama;
	}

	private int hash(K key)
	{ 
		return (key.hashCode() & 0x7fffffff) % tamagno;
	}

	
	public Iterable<K> llaves()
	{
		return listaDeTodasLosElementos().llaves();
	}

	
	public ListaLlaveValorSecuencial<K, V>[] darTabla()
	{
		return tabla;
	}
	
	
	public ListaLlaveValorSecuencial<K, V> listaDeTodasLosElementos()
	{
		ListaLlaveValorSecuencial<K, V> lista = new ListaLlaveValorSecuencial<>();
		for (int i = 0; i < tabla.length; i++) 
		{
			if(i == 0)
			{
				lista = tabla[0];
			}
			else
			{
				if(tabla[i].darCabezaNodo() != null)
				lista.darElementoNodoSimple(lista.darTamanio()-1).setNext(tabla[i].darCabezaNodo());
			}
		}
		return lista;
	}


}

