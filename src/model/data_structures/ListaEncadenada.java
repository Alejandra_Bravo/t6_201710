package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada.NodoDoble;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;

	private NodoSencillo<T> actual;

	public ListaEncadenada()
	{
		primero = new NodoSencillo<T>();
		actual = primero;
	}

	public void agregarAlComienzo(T item)
	{
		if(primero.getItem() == null)
		{
			primero.setItem(item);
		}else
		{
			NodoSencillo<T> temp = new NodoSencillo<>();
			temp.setItem(item);
			temp.setNodoSencillo(primero);
			primero = temp;
		}
	}
	
	public T quitaAlComienzo()
	{
		if(primero.nextNode == null)
		{
			T temp = primero.getItem();
			primero.setItem(null);
			return temp;
		}else 
		{
			T temp = primero.getItem();
			primero = primero.getNextNode();
			return temp;
		}
			
	}


	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>() {
			private NodoSencillo<T> actualT=null;

			public boolean hasNext() {
				if(actualT==null)return primero.getItem()!=null;
				else return actualT.getNextNode() != null;
			}

			public T next() {
				if(actualT==null){
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else{
					actualT = actualT.getNextNode();
					return actualT.getItem();
				}

			}

			@Override
			public void remove() {
				// NA

			}
		};

	} ;		



	@Override
	public void agregarElementoFinal(T elem) {

		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}else
		{
			NodoSencillo<T> a = primero;
			while(a != null)
			{
				if (a.getNextNode()== null)
				{
					a.setNodoSencillo(new NodoSencillo<T>());
					a.getNextNode().setItem(elem);
					actual = a.getNextNode();
					break;
				}
				a = a.getNextNode();
			}

		}
	}

	@Override
	public T darElemento(int pos) {

		actual=primero;
		if(actual!=null){	
			for (int i = 0; i < pos; i++) {
				actual = actual.getNextNode();
			}
			return actual.getItem();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {

		int contador = 0;
		NodoSencillo<T> a = primero;
		while(a!= null && a.getItem()!= null)
		{
			contador ++;
			a = a.getNextNode();
		}		
		return contador;
	}

	@Override
	public T darElementoPosicionActual() {
		if(actual!=null)return actual.getItem();
		else return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() {

		if(actual!=null&&actual.getNextNode() != null)
		{
			actual = actual.getNextNode();
			return true;

		}else{
			return false;
		}

	}

	@Override
	public boolean retrocederPosicionAnterior() {


		NodoSencillo<T> a = primero;
		while(a!=null)
		{
			if (a.getNextNode()!=null&&a.getNextNode().equals(actual))
			{
				actual = a;
				return true;
			}
			a = a.getNextNode();
		}
		return false;


	}


	private class NodoSencillo <T> {

		private T item;

		private NodoSencillo<T> nextNode;

		public NodoSencillo()
		{
			item = null;
			nextNode = null;
		}


		public T getItem() {
			return item;

		}

		public void setItem(T item) {
			this.item = item;
		}

		public NodoSencillo<T> getNextNode() {
			return nextNode;
		}


		public void setNodoSencillo(NodoSencillo<T> nvoNodo) {
			nextNode = nvoNodo;
		}



	}



	@Override
	public void add(T item, int pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void replaceItem(T item, int pos) {
		// TODO Auto-generated method stub
		
	}


}




