package model.data_structures;

public class NodoSimple<K extends Comparable<K>,V> 
{
	
	private K llave ;
	
	private V valor;

	
	private NodoSimple<K,V> siguiente;

	
	public NodoSimple<K,V> darSiguiente()
	{
		return siguiente;
	}

	
	public void cambiarSiguiente(NodoSimple<K,V> i)
	{
		i.siguiente = siguiente;
		siguiente = i;
	}
	

	
	public NodoSimple(K key, V val) 
	{
		llave=key;
		valor=val;
		siguiente = null;
	}
	
	public void setValor(V val)
	{
		valor=val;
	}
	
	public K darLlave()
	{
		return llave;
	}
	
	public V darValor()
	{
		return valor;
	}
	
	public void setNext(NodoSimple<K,V> n)
	{
		this.siguiente = n;
	}

	
	public void desconectarSiguiente()
	{
		siguiente = siguiente.siguiente; 
	}
}