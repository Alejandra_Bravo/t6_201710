package model.data_structures;

public class Queue <T>  {

	ListaDobleEncadenada<T> cola;

	public Queue() {
		cola = new ListaDobleEncadenada<>();		
	}

	public void enqueue(T item) {
		cola.agregarElementoFinal(item);
	}

	public T dequeue() {
		return cola.eliminaPrimeraPos();
	}

	public boolean isEmpty() {
		return (cola.darNumeroElementos() == 0)? true : false ;
	}

	public int size() {
		return cola.darNumeroElementos();
	}

	public T darElemento(int i) {
		return cola.darElemento(i);
	}

	public void eliminaPrimeraPos() {
		cola.eliminaPrimeraPos();
	}

	public void replaceItem(T elemento, int i) {
		cola.replaceItem(elemento, i);
	}



}
