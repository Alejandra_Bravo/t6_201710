package model.data_structures;

public interface Iqueue <T> {

	
	/**
	 * Agrega un item en la �ltima posici�n de la cola
	 * @param item generico
	 */
	public void enqueue(T item);
	
	/**
	 * Elimina el elemento en la primera posici�n de la cola
	 * @return T elemento
	 */
	public T dequeue();
	
	/**
	 * Indica si la cola est� vac�a
	 * @return true si esta vacia, false de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la cola
	 * @return numero de elementos en la cola
	 */
	public int size();

}
