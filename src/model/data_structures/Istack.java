package model.data_structures;

public interface Istack <T> {

	
	/**
	 * Agrega un item al tope de la pila
	 * @param item generico
	 */
	public void push(T item);
	
	/**
	 * Elimina el elemento en el tope de la pila
	 */
	public T pop();
	
	/**
	 * Indica si la pila est� vac�a
	 * @return true si la pila esta vacia, false de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la pila
	 * @return numero de objetos en la pila
	 */
	public int size();

}
