package view;

import java.util.Scanner;


import controller.Controller;

public class VistaManejador {

	public static void main(String[] args) {

		Controller a = new Controller();
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		int option = 0;
		while(!fin){
			printMenu();
			option = sc.nextInt();
			switch(option){
			case 1:
				System.out.println("ingresar numero del tama�o de la tabla hash");
				String ingresado=sc.next();
				int n = Integer.parseInt(ingresado);
				a.crearManejadorPelis(n);;
				break;
			case 2:
				System.out.println("ingresar id de la pelicula");
				String ingresado1=sc.next();
				long longs = Long.parseLong(ingresado1);
				a.buscarPelicula(longs);
				break;
			case 3:
				System.out.println("El promedio de objetos por casillas es: "+a.darPromedio());
				break;
			case 4:
				fin=true;
				sc.close();
				break;				
			}

		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Crear el manajador de peliculas");
		System.out.println("2. Buscar una pelicula por su ID");
		System.out.println("3. Consultar el promedio de objetos por posici�n en el arreglo");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}

}
